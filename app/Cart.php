<?php

namespace App;

use App\Fabric;
use App\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class Cart
{
    public $products = [];
    public $totalProducts;
    public $totalPrice;

    public function __construct($cart = null)
    {
        if ($cart) {
            $this->products = $cart->products;
            $this->totalProducts = $cart->totalProducts;
            $this->totalPrice = $cart->totalPrice;
        }
        else{
            $this->products = [];
            $this->totalProducts = 0;
            $this->totalPrice = 0;
        }

    }

    //baigta
    public function add($request)
    {
        $meters = $request->meters;
        $id = $request->fabricId;
        $fabric = Fabric::findOrFail($id);
        $request->validate(
            ['meters' => ['required','numeric','min:1', "max:{$fabric->meters_left}"]]
        );
//        if (session()->has('cart')){
//            $cart = new Cart(session()->get('cart'));
//        }
        $product = [
            'id' => $fabric['id'],
            'name' => $fabric['name'],
            'meters' => $meters,
            'cost' => $fabric['cost'] * $meters,
            'photo' => $fabric['photo'],
        ];
        if (session()->has('cart'))
        {
            $data = session()->get('cart');
            if (!array_key_exists($fabric['id'] , $data->products)){
                $data->products[$fabric['id']] = $product;
                $data->totalProducts += 1;
                $data->totalPrice += $fabric['cost'] * $meters;
            }
            else{
                $data->products[$fabric['id']]['meters'] += $meters;
                $data->products[$fabric['id']]['cost'] += $fabric['cost'] * $meters;
                $data->totalPrice += $fabric['cost'] * $meters;
            }
            session()->put('cart', $data);
        }
        else
        {
            $this->products[$fabric['id']] = $product;
            $this->totalProducts = 1;
            $this->totalPrice = $fabric['cost'] * $meters;
            session()->put('cart', $this);
        }
    }
    public function update($request)
    {
        $order = $request->order;
        $checkMetersRequest = new Request([
            'meters' => $order['meters'],
        ]);
        if (session()->has('cart'))
        {
            $id = $order['id'];
            $fabric = Fabric::findOrFail($id);
            $data = session()->get('cart');
            $checkMetersRequest->validate(
                ['meters' => ['required','numeric','min:1', "max:{$fabric->meters_left}"]]
            );
            $data->totalPrice -= $data->products[$id]['cost'];
            $data->products[$id]['meters'] = $order['meters'];
            $data->products[$id]['cost'] = $fabric['cost'] * $order['meters'];
            $data->totalPrice += $data->products[$id]['cost'];
            session()->put('cart', $data);
        }
    }
    public function removeItem($id)
    {
        if (session()->has('cart'))
        {
            $data = session()->get('cart');
            foreach ($data->products as $product)
            {
                if($product['id'] == $id)
                {
                    $data->totalProducts -= 1;
                    $data->totalPrice -= $data->products[$id]['cost'];
                    unset($data->products[$id]);
                }
            }
            if($data->totalProducts <= 0){
                session()->forget('cart');
            }
            else{
                session()->put('cart', $data);
            }
        }
    }
    public function getTotalProductsNumber()
    {
        if (session()->has('cart')){
            return response()->json(session()->get('cart')->totalProducts);
        }
    }
    public function getCart()
    {
        if (!session()->has('cart')) {
            return response()->json([
                'status'   => 'error',
                'message'  => 'Nėra prekiu'
            ], 400); //404?
        }
        return response()->json(session()->get('cart'));
    }
    // sutvarkyti front-end dali
    public function orderProducts($request)
    {
        if (auth::check())
        {
            $data = session()->get('cart');
            $date = date("Y-m-d", strtotime($request->date));
            $today = date('Y-m-d');
            foreach ($data->products as $product)
            {
                $fabric = Fabric::findOrFail($product['id']);
                $checkMetersRequest = new Request([
                    'meters' => $product['meters'],
                ]);
                $validator = Validator::make($checkMetersRequest->all(), [
                    'meters' => ['required','numeric','min:1', "max:{$fabric->meters_left}"]
                ]);
                if (!$validator->fails())
                {
                    $fabric->meters_left -= $product['meters'];
                    $fabric->save();
                    $order = new Order();
                    $order->order_date = $today;
                    $order->delivery_date = $date;
                    $order->delivered = 0;
                    $order->quantity = 1;
                    $order->state = Order::STATUS_WAITING;
                    $order->price = $product['cost'];
                    $order->meters = $product['meters'];
                    $order->user_id = auth::id();
                    $order->fabric_id = $product['id'];
                    $order->shipper_id = 1;
                    $order->save();
                    //pataisyti - isimti funkcija ir sukurti funkcija kuri paliktu audinius kurie nepraejo validacija
                    $this->removeItem($product['id']);
                    //
                    $data->totalProducts =- 1;
                }
            }
            if (session()->has('cart'))
            {
                //dar pataisyti
                return response()->json("Kai kurios prekes nebuvo uzsakytos");
            }
            else
            {
                session()->forget('cart');
                return response()->json("Prekės užsakytos");
            }
        }
        else
        {
            return response()->json([
                'status'   => 'error',
                'message'  => 'Neprisijunges'
            ],403);
        }
    }
    public function clear(){
        session()->forget('cart');
    }
}
