<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Fabric extends Model
{
    protected $table = 'fabrics';
    protected $primaryKey = 'id';
    public $timestamps = false;

//    protected $fillable = [
//        "name",
//        "type",
//        "width",
//        "cost",
//        "color",
//        "meters_left",
//        "composition",
//        "comment",
//        "photo"
//    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
