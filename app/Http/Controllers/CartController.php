<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Fabric;
use App\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class CartController extends Controller
{
    public function addToCart(Request $request){
        $cart = new Cart();
        $cart->add($request);
        return response()->json(session()->get('cart')->totalProducts);
    }
    public function updateOrder(Request $request)
    {
        $cart = new Cart();
        $cart->update($request);
        return response()->json("Pakeista");
    }
    public function removeItemFromCart($id)
    {
        $cart = new Cart();
        $cart->removeItem($id);
    }
    public function getTotalProducts()
    {
        $cart = new Cart();
        return $cart->getTotalProductsNumber();
    }
    public function getCart()
    {
        $cart = new Cart();
        return $cart->getCart();
    }
    public function order(Request $request)
    {
        $cart = new Cart();
        return $cart->orderProducts($request);
    }
}
