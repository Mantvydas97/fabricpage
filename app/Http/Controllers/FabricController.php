<?php

namespace App\Http\Controllers;

use App\Fabric;
use Illuminate\Http\Request;

class FabricController extends Controller
{
    public function getAll()
    {
        $data = Fabric::All();
        return response()->json($data);
    }

    public function getAllWithRelations()
    {
        $data = Fabric::with("orders")->get();
        return response()->json($data);
    }

    public function getById($id)
    {
        $data = Fabric::where('id', $id)->first();
        return response()->json($data);
    }

    public function getByIdWithRelations($id)
    {
        $data = Fabric::where('id', $id)->with("orders")->first();
        return response()->json($data);
    }

    public function insert(Request $request)
    {
        $request->validate([
            "name" => "required",
            "type" => "required",
            "width" => "required",
            "cost" => "required",
            "meters_left" => "required",
            "color" => "required",
            "composition" => "required",
            "comment" => "required",
            "photo" => "required"
        ], [
            'name.required' => "Fill name input",
            'type.required' => "Fill type input",
            'width.required' => "Fill width input",
            'cost.required' => "Fill cost input",
            'meters_left.required' => "Fill meters left input",
            'color.required' => "Fill color input",
            'composition.required' => "Fill composition input",
            'comment.required' => "Fill comment input",
        ]);
        $fabric = new Fabric();
        $fabric->name = $request->name;
        $fabric->type = $request->type;
        $fabric->width = $request->width;
        $fabric->cost = $request->cost;
        $fabric->meters_left = $request->meters_left;
        $fabric->color = $request->color;
        $fabric->composition = $request->composition;
        $fabric->comment = $request->comment;
        if($request->get('photo'))
        {
            $image = $request->get('photo');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('photo'))->save(public_path('images/').$name);
        }
        $fabric->photo = $name;
        $fabric->save();
        return response()->json(['success' => 'Ikelta'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "name" => "alpha",
            "type" => "alpha",
            "width" => "number",
            "cost" => "number",
            "meters_left" => "number",
            "color" => "alpha",
        ]);
        $data = Fabric::findOrFail($id);
        if($request->name)
            $data->name = $request->name;
        if($request->type)
            $data->type = $request->type;
        if($request->width)
            $data->width = $request->width;
        if($request->cost)
            $data->cost = $request->cost;
        if($request->meters_left)
            $data->meters_left = $request->meters_left;
        if($request->color)
            $data->color = $request->color;
        if($request->composition)
            $data->composition = $request->composition;
        if($request->comment)
            $data->comment = $request->comment;
        if($request->photo)
            $data->photo = $request->photo;
        $data->save();
        return $data;
    }

    public function deleteById($id)
    {
        $data = Fabric::findOrFail($id);
        $data->delete();
        return "Fabric deleted";
    }
}
