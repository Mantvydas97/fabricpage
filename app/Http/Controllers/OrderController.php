<?php

namespace App\Http\Controllers;


use App\Fabric;
use App\Order;

use App\Shipper;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function getAll(){
        $data = Order::All();
        return response()->json($data);
    }
    public function getById($id){
        $data = Order::where('id', $id)->first();
        return response()->json($data);
    }
    public function insert(Request $request){
        $data = $request->validate([
            'order_date' => 'required',
            'delivery_date' => 'required',
            'delivered' => 'required',
            'quantity' => 'required',
            'price' => 'required',
            'meters' => 'required',
            'user_id' => 'required',
            'fabric_id' => 'required',
            'shipper_id' => 'required',
            'state' => 'required',
        ],
            [
                'order_date.required' => 'Fill order date input',
                'delivery_date.required' => 'Fill delivery date input',
                'price.required' => 'Fill price input',
                'meters.required' => 'Fill meters input',
                'user_id.required' => 'Fill user input',
                'fabric_id.required' => 'Fill fabric input',
                'shipper_id.required' => 'Fill shipper input',
                'state.required' => 'Fill state input'
            ]);
        $order = new Order();
        $order->order_date = $data['order_date'];
        $order->delivery_date = $data['delivery_date'];
        $order->delivered = $data['delivered'];
        $order->quantity = $data['quantity'];
        $order->price = $data['price'];
        $order->meters = $data['meters'];
        $order->user_id = $data['user_id'];
        $order->fabric_id = $data['fabric_id'];
        $order->shipper_id = $data['shipper_id'];
        $order->state = $data['state'];
        $order->save();
        return $order;
    }
    public function update(Request $request, $id){
        $data = Order::findOrFail($id);
        $request->validate([
            "order_date" => "date",
            "delivery_date" => "date",
            "state" => "number",
            "meters" => "number",
            "price" => "number",
            "quantity" => "number",
            "user_id" => "number",
            "fabric_id" => "number",
            "shipper_id" => "number",
        ]);
        if($request->order_date)
            $data->order_date = $request->order_date;
        if($request->delivery_date)
            $data->delivery_date = $request->delivery_date;
        if($request->state)
            $data->state = $request->state;
        if($request->meters)
            $data->meters = $request->meters;
        if($request->price)
            $data->price = $request->price;
        if($request->quantity)
            $data->quantity = $request->quantity;
        if($request->user_id)
            $data->user_id = $request->user_id;
        if($request->fabric_id)
            $data->fabric_id = $request->fabric_id;
        if($request->shipper_id)
            $data->shipper_id = $request->shipper_id;
        $data->delivered = 0;
        $data->save();
        return $data;
    }
    public function deleteById($id){
        $data = Order::findOrFail($id);
        $data->delete();
        return "Order deleted";
    }
    public function groupOrders(){
        $orders = Order::All();
        $collection = collect();
        foreach ($orders as $order) {
            if($collection->contains('orderDate',$order->order_date) && $collection->contains('deliveryDate',$order->delivery_date) && $collection->contains('user', $order->user_id)){

                $get = $collection->where('orderDate', $order->order_date)->where('deliveryDate', $order->delivery_date)->first();
                $get['quantity']++;
                $get['totalPrice'] += $order->price;
                array_push($get['products'],$order->fabric_id );
                array_push($get['orders'],Order::where('id', $order->id)->first());
                $collection = $collection->replace([$collection->where('orderDate', $order->order_date)->where('deliveryDate', $order->delivery_date)->keys()->first() => $get]);
                continue;
            }
            else {
                $collection->push(['orderDate' => $order->order_date, 'deliveryDate' => $order->delivery_date , 'quantity' => 1 , 'totalPrice' => $order->price, 'state' => $order->state , 'products' => [$order->fabric_id], 'orders' => [Order::where('id', $order->id)->first()] , 'user' => $order->user_id]);
            }
        }
        return $collection;
    }
    function confirmOrder(Request $request)
    {
        $orderConfirm = $request->order;
        foreach ($orderConfirm['orders'] as $toConfirm) {
            $state = 2;
            $data = Order::findOrFail($toConfirm);
            $data->state = $state;
            $data->save();
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Patvirtinta'
        ], 200);
    }
    public function updateGroupOrder(Request $request){
        $fullOrder = $request->order;
        $orderDate = date("Y-m-d", strtotime($request->orderDate));
        $deliveryDate = date("Y-m-d", strtotime($request->deliveryDate));
        $state = $request->state;
        $shipper = $request->shipper;
        foreach ($fullOrder['orders'] as $order) {
            $original = Order::where('id' , $order['id'])->first();
            if(Fabric::where('id' , $order['fabric_id']) != null){
                $original->meters = $order['meters'];
                $original->price = $order['price'];
                $original->fabric_id = $order['fabric_id'];
                $original->order_date = $orderDate;
                $original->delivery_date = $deliveryDate;
                $original->state = $state;
                $original->shipper_id = Shipper::where('name' , $shipper)->first()->id;
                $original->save();
            }
        }
        return response()->json([
            'status' => 'success',
            'message' => 'working'
        ]);
    }
}
