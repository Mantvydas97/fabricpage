<?php

namespace App\Http\Controllers;

use App\Fabric;
use App\Order;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function getUserOrder(){
        $orders = auth()->user()->orders;
        $collection = collect();
        foreach ($orders as $order) {
            if($collection->contains('orderDate',$order->order_date) && $collection->contains('deliveryDate',$order->delivery_date)){

                $get = $collection->where('orderDate', $order->order_date)->where('deliveryDate', $order->delivery_date)->first();
                $get['quantity']++;
                $get['totalPrice'] += $order->price;
                array_push($get['products'],$order->fabric_id );
                array_push($get['orders'],$order->id);
                $collection = $collection->replace([$collection->where('orderDate', $order->order_date)->where('deliveryDate', $order->delivery_date)->keys()->first() => $get]);
                continue;
            }
            else {
                $collection->push(['orderDate' => $order->order_date, 'deliveryDate' => $order->delivery_date , 'quantity' => 1 , 'totalPrice' => $order->price, 'state' => $order->state , 'products' => [$order->fabric_id], 'orders' => [$order->id]]);
            }
        }
        return $collection;
    }
    function cancelOrder(Request $request) {
        $orders = auth()->user()->orders;
        $orderCancel = $request->order;
        foreach ($orders as $order) {
            foreach ($orderCancel['orders'] as $toCancel) {
                if($order->id == $toCancel){
                    //$order->state = 3;
                    $order->state = Order::STATUS_CANCELLED;
                    $data = Order::findOrFail($toCancel);
                    $fabric = Fabric::findOrFail($order->fabric_id);
                    if ($data->state == Order::STATUS_DELIVERING || $data->state == Order::STATUS_DELIVERED || $data->state == ORDER::STATUS_CANCELLED){
                        return response()->json([
                            'status'   => 'error',
                            'message'  => 'Užsakymas negali buti atsaukiamas kai yra pristatytas, vezamas ar atsauktas'
                        ],422);
                    }
                    $data->state = $order->state;
                    $fabric->meters_left = $fabric['meters_left'] + $order['meters'];
                    $data->save();
                    $fabric->save();
                }
            }
        }
        return response()->json([
            'status'   => 'success',
            'message'  => 'Užsakymas atšauktas'
        ],200);
    }
}
