<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shipper;

class ShipperController extends Controller
{
    public function getAll(){
        $data = Shipper::All();
        return response()->json($data);
    }
    public function getAllWithRelations(){
        $data = Shipper::with("orders")->get();
        return response()->json($data);
    }
    public function getById($id){
        $data = Shipper::where('id', $id)->first();
        return response()->json($data);
    }
    public function getByIdWithRelations($id){
        $data = Shipper::where('id', $id)->with("orders")->first();
        return response()->json($data);
    }
    public function insert(Request $request){
        $request->validate([
            'name' => 'required|alpha',
            'surname' => 'required|alpha',
            'phone' => 'required',
        ]);
        $shipper = new Shipper();
        $shipper->name = $request->name;
        $shipper->surname = $request->surname;
        $shipper->phone = $request->phone;
        $shipper->save();
        return $shipper;
        //return Shipper::create($request->all());
    }
    public function update(Request $request, $id){
        $request->validate([
            'name' => 'alpha',
            'surname' => 'alpha',
            'phone' => 'numeric',
        ]);
        $data = Shipper::findOrFail($id);
        if($request->name)
        $data->name = $request->name;
        if($request->surname)
        $data->surname = $request->surname;
        if($request->phone)
        $data->phone = $request->phone;
        $data->save();
        return $data;
    }
    public function deleteById($id){
        $data = Shipper::findOrFail($id);
        $data-> delete();
        return "Shipper deleted";
    }
}
