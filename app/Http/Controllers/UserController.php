<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function getAll(){
        $data = User::All();
        return response()->json($data);
    }
    public function getAllWithRelations(){
        $data = User::with("orders")->get();
        return response()->json($data);
    }
    public function getById($id){
        $data = User::where('id', $id)->first();
        return response()->json($data);
    }
//    public function getByIdWithRelations($id){
//        $data = User::where('id', $id)->with("orders")->first();
//        return response()->json($data);
//    }
    public function insert(Request $request){
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required',
            'address' => 'required',
            'phone' => 'required',
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->role = $request->role;
        $user->email_verified_at = $request->email_verified_at;
        //$user->remember_token = Str::random(10);
        $user->save();
        return $user;
    }
    public function update(Request $request, $id){
        $request->validate([
            'name'=>'alpha',
            'email'=>'email',
            'password'=>'password',
            'address' => 'alpha',
        ]);
        $data = User::findOrFail($id);
        if($request->name)
        $data->name = $request->name;
        if($request->email)
        $data->email = $request->email;
        if($request->address)
        $data->address = $request->address;
        if($request->phone)
        $data->phone = $request->phone;
        if($request->role)
        $data->role = $request->role;
        $data->save();
        return $data;
    }
    public function deleteById($id){
        $data = User::findOrFail($id);
        $data-> delete();
        return "User deleted";
    }
}
