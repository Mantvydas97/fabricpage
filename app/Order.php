<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'id';
    public $timestamps = false;

    const STATUS_WAITING = 0;
    const STATUS_DELIVERING = 1;
    const STATUS_DELIVERED = 2;
    const STATUS_CANCELLED = 3;

//    protected $fillable = [
//        'order_date',
//        'delivery_date',
//        'delivered',
//        'state',
//        'quantity',
//        'price',
//        'meters',
//        'user_id',
//        'fabric_id',
//        'shipper_id'
//    ];
    /* states
     * 0 = waiting
     * 1 = being delivered
     * 2 = delivered
     * 3 = cancelled
     */

    public function client(){

        return $this->belongsTo(User::class);
    }
    public function fabric(){

        return $this->belongsTo(Fabric::class);
    }
    public function shipper(){

        return $this->belongsTo(Shipper::class);
    }
}
