<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipper extends Model
{
    protected $table = 'shippers';
    protected $primaryKey = 'id';
    public $timestamps = false;


//    protected $fillable = [
//        'name',
//        'surname',
//        'phone'
//    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
