<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Fabric;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Fabric::class, function (Faker $faker) {
    $arrayTypes = array("Acetatas", "Akrilas", "Džinsas", "Kašmyras", "Medvlinė", "Šilkas");
    $index = array_rand($arrayTypes);
    return [
        'name' => Str::random(10),
        'type' => $arrayTypes[$index],
        'width' => $faker->numberBetween(150,200),
        "cost" => $faker->numberBetween(4,10),
        "meters_left" => 200,
        "color" => $faker->colorName,
        "composition" => "...",
        "comment" => $faker->realText($faker->numberBetween(100,255)),
        "photo" => $faker->numberBetween($min = 1, $max = 7) . ".jpg",
    ];
});
