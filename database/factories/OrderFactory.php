<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        "order_date" => date("Y-m-d"),
        "delivery_date" => $faker->date($format = 'Y-m-d', $max = 'now'),
        "delivered" => $faker->boolean,
        "state" => 0,
        "meters" => $faker->numberBetween(1,20),
        "price" => $faker->numberBetween(10,100),
        "user_id" => App\User::inRandomOrder()->first()->id,
        "fabric_id" => App\Fabric::inRandomOrder()->first()->id,
        "shipper_id" => App\Shipper::inRandomOrder()->first()->id,
    ];
});
