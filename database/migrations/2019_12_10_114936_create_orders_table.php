<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('order_date');
            $table->date('delivery_date');
            $table->boolean('delivered');
            $table->integer('state');
            $table->decimal('meters', 5,2);
            $table->decimal('price', 5,2);
            $table->bigInteger('quantity')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();;
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('fabric_id')->unsigned()->nullable();;
            $table->foreign('fabric_id')->references('id')->on('fabrics')->onDelete('set null');
            $table->bigInteger('shipper_id')->unsigned()->nullable();;
            $table->foreign('shipper_id')->references('id')->on('shippers')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
