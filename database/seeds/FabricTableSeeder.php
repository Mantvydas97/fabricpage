<?php

use Illuminate\Database\Seeder;

class FabricTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Fabric::class, 20)->create();
    }
}
