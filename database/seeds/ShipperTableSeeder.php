<?php

use Illuminate\Database\Seeder;

class ShipperTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Shipper::class, 1)->create();
    }
}
