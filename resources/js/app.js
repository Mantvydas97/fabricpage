import Vue from 'vue';
import './bootstrap'
import router from './router';
import VueRouter from 'vue-router'
import App from './components/App'
import axios from 'axios';
import VueAuth from '@websanova/vue-auth';
import VueAxios from 'vue-axios';
import auth from './auth';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
// import Datepicker from 'vuejs-datepicker';
// import moment from 'moment'

//require('./bootstrap');

//window.Vue = require('vue');

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.router = router;
Vue.use(VueRouter);
Vue.use(VueSweetalert2);
// Vue.use(moment);

Vue.use(VueAxios, axios);
axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`;
Vue.use(VueAuth, auth);

Vue.component('app', App);

const app = new Vue({
    el: '#app',
    router
});
