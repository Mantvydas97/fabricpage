import Vue from 'vue';
import VueRouter from 'vue-router';
import ExampleComponent from "./components/Main";
import Register from "./components/Register";
import Login from "./components/Login";
import AdminDashboard from "./components/Admin/AdminDashboard";
import Products from "./components/Products";
import Cart from "./components/Cart";
import Profile from "./components/Profile";
import Fabrics from "./components/Admin/Fabrics";
import Users from "./components/Admin/Users";
import Orders from "./components/Admin/Orders";
import Shippers from "./components/Admin/Shippers";
import PostUser from "./components/Admin/PostUser";
import PostFabric from "./components/Admin/PostFabric";
import PostOrder from "./components/Admin/PostOrder";
import PostShipper from "./components/Admin/PostShipper";
import GroupedOrders from "./components/Admin/GroupedOrders";

  const routes = [
      {
          path: '/',
          name: 'home',
          component: ExampleComponent,
          meta: {
              auth: undefined
          }
      },
      {
          path: '/registracija',
          name: 'register',
          component: Register,
          meta: {
              auth: false
          }
      },
      {
          path: '/prisijungimas',
          name: 'login',
          component: Login,
          meta: {
              auth: false
          }
      },
      {
          path: '/produktai',
          name: 'products',
          component: Products,
          meta: {
              auth: undefined
          }
      },
      {
          path: '/krepselis',
          name: 'cart',
          component: Cart,
          meta: {
              auth: undefined
          }
      },
      {
          path: '/profilis',
          name: 'profile',
          component: Profile,
          meta: {
              auth: true
          }
      },
      //admins
      {
          path: '/admin/dashboard',
          name: 'admin',
          component: AdminDashboard,
          meta: {
              auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: {name: 'home'}}
          }
      },
      {
          path: '/admin/fabrics',
          name: 'adminFabrics',
          component: Fabrics,
          meta: {
              auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: {name: 'home'}}
          }
      },
      {
          path: '/admin/users',
          name: 'adminUsers',
          component: Users,
          meta: {
              auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: {name: 'home'}}
          }
      },
      {
          path: '/admin/orders',
          name: 'adminOrders',
          component: Orders,
          meta: {
              auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: {name: 'home'}}
          }
      },
      {
          path: '/admin/shippers',
          name: 'adminShippers',
          component: Shippers,
          meta: {
              auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: {name: 'home'}}
          }
      },
      {
          path: '/admin/postUser',
          name: 'postUser',
          component: PostUser,
          meta: {
              auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: {name: 'home'}}
          }
      },
      {
          path: '/admin/postFabric',
          name: 'postFabric',
          component: PostFabric,
          meta: {
              auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: {name: 'home'}}
          }
      },
      {
          path: '/admin/postOrder',
          name: 'postOrder',
          component: PostOrder,
          meta: {
              auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: {name: 'home'}}
          }
      },
      {
          path: '/admin/postShipper',
          name: 'postShipper',
          component: PostShipper,
          meta: {
              auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: {name: 'home'}}
          }
      },
      {
          path: '/admin/group',
          name: 'groupedOrders',
          component: GroupedOrders,
          meta: {
              auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: {name: 'home'}}
          }
      },
    ];
    const router = new VueRouter({
        history: true,
        mode: 'history',
        routes,
    });

    export default router
