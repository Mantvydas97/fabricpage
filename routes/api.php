<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');
    });
});
Route::group(['middleware' => 'auth:api'], function () {
    // Users
//    Route::get('users', 'UserController@index')->middleware('isAdmin');
//    Route::get('users/{id}', 'UserController@show')->middleware('isAdminOrSelf');

    //Users
    Route::get('user/all', 'UserController@getAll')->middleware('isAdmin');
    Route::get('user/relation/all', 'FabricController@getAllWithRelations');
    Route::get('user/{id}', "UserController@getById")->middleware('isAdminOrSelf');
    Route::get('user/relation/{id}', 'UserController@getByIdWithRelations')->middleware('isAdminOrSelf');
    Route::post('user/post', 'UserController@insert')->middleware('isAdmin');
    Route::put('user/{id}', 'UserController@update')->middleware('isAdmin');
    Route::delete('user/{id}', "UserController@deleteById")->middleware('isAdmin');
    //Fabric
    Route::post('fabric/post', 'FabricController@insert')->middleware('isAdmin');;
    Route::put('fabric/{id}', 'FabricController@update')->middleware('isAdmin');;
    Route::delete('fabric/{id}', "FabricController@deleteById")->middleware('isAdmin');;
    //Shipper
    Route::post('shipper/post', 'ShipperController@insert')->middleware('isAdmin');;
    Route::put('shipper/{id}', 'ShipperController@update')->middleware('isAdmin');;
    Route::delete('shipper/{id}', "ShipperController@deleteById")->middleware('isAdmin');;
    Route::get('shipper/all', 'ShipperController@getAll')->middleware('isAdmin');;
    Route::get('shipper/{id}', "ShipperController@getById")->middleware('isAdmin');;
    //Order
    Route::get('order/all', 'OrderController@getAll')->middleware('isAdmin');
    Route::get('order/group', 'OrderController@groupOrders')->middleware('isAdmin');
    Route::get('order/{id}', "OrderController@getById")->middleware('isAdminOrSelf');
    Route::post('order/post', 'OrderController@insert')->middleware('isAdmin');
    Route::put('order/confirm', 'OrderController@confirmOrder')->middleware('isAdmin');
    Route::put('order/{id}', 'OrderController@update')->middleware('isAdmin');
    Route::post('order/update', 'OrderController@updateGroupOrder')->middleware('isAdmin');
    Route::delete('order/{id}', "OrderController@deleteById")->middleware('isAdmin');

});

// Fabric
Route::get('fabric/all', 'FabricController@getAll');
Route::get('fabric/{id}', "FabricController@getById");
//Shipper

// Cart
Route::post('cart/post', 'CartController@addToCart');
Route::post('cart/order', 'CartController@order');
Route::put('cart/update', 'CartController@updateOrder');
Route::get('cart/getCart', 'CartController@getCart');
Route::get('cart/getTotal', 'CartController@getTotalProducts');
Route::delete('cart/removeProduct/{id}', 'CartController@removeItemFromCart');
// Order

// Profile
Route::get('profile/getOrder', 'ProfileController@getUserOrder');
Route::post('profile/cancelOrder', 'ProfileController@cancelOrder');

//Test Cart
Route::post('cart/testPost', 'CartController@testingAddCart');
